package u03

import scala.annotation.tailrec

sealed trait List[E]
sealed trait Option[E] // An Optional data type
sealed trait Person

object EXLab03 extends App {

  case class Cons[E](head: E, tail: List[E]) extends List[E]
  case class Nil[E]() extends List[E]

  case class Some[E](t:E) extends Option[E]
  case class None[E]() extends Option[E]

  case class Student(name: String, year:Int) extends Person
  case class Teacher(name: String, course:String) extends Person

  val  lst = Cons(10, Cons(20, Cons(30, Nil())))

  //1a)
  @tailrec
  def drop[A](l: List[A], n: Int): List[A] = l match {
    case Cons(_, t) if n>0=> drop(t, n - 1)
    case _=> l
  }
  println(drop(lst ,1) )// Cons(20,Cons(30, Nil()))
  println(drop(lst ,2)) // Cons(30, Nil())
  println(drop(lst ,5)) // Nil()

  //1b
  def append[A](l1: List[A], l2: List[A]): List[A] = (l1, l2) match {
    case (Cons(h, t), l2) => Cons(h, append(t, l2))
    case _ => l2
  }
  def flatMap[A,B](l:List[A])(f: A => List[B]): List[B]= l match{
    case Cons(h,t) => append(f(h), flatMap(t)(f))
    case Nil() => Nil()
  }
  println(flatMap(lst)(v => Cons(v+1, Nil()))) // Cons(11,Cons(21,Cons(31,Nil())))
  println(flatMap(lst)(v => Cons(v+1, Cons(v+2, Nil()))))// Cons(11,Cons(12,Cons(21,Cons(22,Cons(31,Cons(32,Nil())))))


  //1c
  def map[A,B](l: List[A])(mapper: A=>B): List[B] = flatMap(l)(a=> Cons(mapper(a),Nil()))

  //1d
  def filter[A](l1: List[A])(p: A=>Boolean): List[A]= flatMap(l1)(a=> {if(p(a)) Cons(a, Nil()) else Nil()})

  //2
  def max(l: List[Int]): Option[Int] = {
    @tailrec
    def m(l1:List[Int] ,op:Option[Int]): Option[Int]=(l1,op) match{
      case(Cons(h,t), Some(value)) => m(t, Some(Math.max(h, value)))
      case (Cons(h,t), None())=> m(t, Some(h))
      case _=>op
    }
    m(l, None())
  }
  println(max(Cons(10, Cons(25, Cons(20, Nil()))))) //Some(25)
  println(max(Nil())) //None()

  //es 3
  def teacherCor(p:List[Person]):List[String] = {
    map(filter[Person](p)(pp=>pp.isInstanceOf[Teacher])) (_ match{
      case Teacher(_, course)=> course
      case _ =>""
    })
  }
  val person: List[Person] = Cons(Teacher("DocenteA", "OOP"), Cons(Student("StudenteA", 1997), Cons(Teacher("DocenteB", "IOT"),Cons(Student("StudenteB", 1999), Nil()))))
  println(teacherCor(person))

  //es4
  @tailrec
  def foldLeft[A,B](l: List[A])(n:B)(mapper: (B,A)=>B): B= l match {
    case Cons(h,t) =>foldLeft(t)(mapper(n,h))(mapper)
    case Nil()=> n
  }
  val lst1= Cons(3, Cons(7, Cons(1, Cons(5, Nil()))))
  println(foldLeft(lst1)(0)(_-_))

  def foldRight[A,B](l: List[A])(n:B)(mapper1: (A,B)=>B): B = l match{
    case Cons(h,t)=>mapper1(h,foldRight(t)(n)(mapper1))
    case Nil()=>n
  }
  val lst2= Cons(3, Cons(7, Cons(1, Cons(5, Nil()))))
  println(foldRight(lst2)(0)(_-_))

}
