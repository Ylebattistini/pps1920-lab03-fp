package u03

import u03.Streams.Stream

import scala.annotation.tailrec

object Streams {
  import Lists._
  sealed trait Stream[A]

  object Stream {

    private case class Empty[A]() extends Stream[A]

    private case class Cons[A](head: () => A, tail: () => Stream[A]) extends Stream[A]

    def empty[A](): Stream[A] = Empty()

    def cons[A](hd: => A, tl: => Stream[A]): Stream[A] = {
      lazy val head = hd
      lazy val tail = tl
      Cons(() => head, () => tail)
    }

    def toList[A](stream: Stream[A]): List[A] = stream match {
      case Cons(h, t) => List.Cons(h(), toList(t()))
      case _ => List.Nil()
    }

    def map[A, B](stream: Stream[A])(f: A => B): Stream[B] = stream match {
      case Cons(head, tail) => cons(f(head()), map(tail())(f))
      case _ => Empty()
    }

    def filter[A](stream: Stream[A])(p: A => Boolean): Stream[A] = stream match {
      case Cons(head, tail) if p(head()) => cons(head(), filter(tail())(p))
      case Cons(_, tail) => filter(tail())(p)
      case _ => Empty()
    }

    def take[A](stream: Stream[A])(n: Int): Stream[A] = (stream, n) match {
      case (Cons(head, tail), n) if n > 0 => cons(head(), take(tail())(n - 1))
      case _ => Empty()
    }

    def iterate[A](init: => A)(next: A => A): Stream[A] = cons(init, iterate(next(init))(next))

    //es5
    @tailrec
    def dropS[A](stream: Stream[A])(n: Int): Stream[A] = (stream, n) match {
      case (Cons(_, t), n) if n > 0 => dropS(t())(n - 1)
      case _ => stream
    }

    //es6
    def constant[A](k: A): Stream[A] = {
      lazy val tail: Stream[A] = Cons(() => k, () => tail)
      tail
    }

    //es7
    /*FIRST IMPLEMENTATION def fibonacciS():Stream[Int]= map(iterate((0,1))(f=>(f._1+f._2, f._1)))(_._1)*/
    def fibonacciS(): Stream[Int] = cons(0, map(iterate((0, 1))(t => (t._2, t._1 + t._2)))(t => t._2))
  }
}

object StreamsMain extends App {
  // var simplifies chaining of functions a bit..
  var str = Stream.iterate(0)(_+1)   // {0,1,2,3,..}
  str = Stream.map(str)(_+1)    // {1,2,3,4,..}
  str = Stream.filter(str)(x => x < 3 || x > 20) // {1,2,21,22,..}
  str = Stream.take(str)(10) // {1,2,21,22,..,28}
  println(Stream.toList(str)) // [1,2,21,22,..,28]

  val s=Stream.take(Stream.iterate(0)(_+1))(10)
  println(Stream.toList(Stream.dropS(s)(6)))

  println(Stream.toList(Stream.take(Stream.constant("x"))(5)))

  val fibs= Stream.fibonacciS()
  println(Stream.toList(Stream.take(fibs)(8)))

  /*val corec: Stream[Int] = Stream.cons(1, corec) // {1,1,1,..}
  println(Stream.toList(Stream.take(corec)(10))) // [1,1,..,1]*/
}
